package ch.netwerkk.alemania;

import java.io.IOException;

/**
 * Created by Mischa.Imbiscuso on 15.12.2016.
 *
 * Main Klasse, hier wird nur das Programm gestartet. Keine Funktion vorhanden. Konstruktor von ReadFile
 * fuehrt die korrekte Methode aus. Der Pfad der Datei wird angegeben und dem Objekt ReadFile mitgegeben
 *
 *
 */
public class FileData {

    /**
     *
     */

    //Pfad der Logon TXT Datei
    private static final String logPath = "//192.168.1.16/Logs$/Logons.txt";

    public static void main(String[] args) throws IOException {
        ReadFile readfile = new ReadFile(logPath);
    }
}
