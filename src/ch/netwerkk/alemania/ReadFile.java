package ch.netwerkk.alemania;

import java.io.*;
import java.io.File;

/**
 * Created by Mischa.Imbiscuso on 15.12.2016.
 * <p>
 * Programm, welches die Logon.txt file einliest. Pfad kommt von der main Methode aus FileData
 */
public class ReadFile {


    // Lokale Variabel vom Dateipfad
    private String fileName;
    ParseWeekdays parse;


    /**
     * Konstruktor - Pfad wird definiert und Methode readFile ausgefuehrt
     *
     * @param file_path
     */

    public ReadFile(String file_path) {

        this.fileName = file_path;
        try {
            readFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ReadFile liest die Datei ein.
     * Jede Linie von der Datei wird als String geparst und dem Array in der Klasse ParseWeekdays hinzugefuegt
     * Danach wird in dieser Methode, die Methode findWeek und writeToCSV von der Klasse PaseWeekdays ausgefuehrt
     *
     * @throws IOException
     */
    public void readFile() throws IOException {
        File file = new File(this.fileName);
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        DataInputStream dis = null;

        try {
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            String strLine;
            parse = new ParseWeekdays();
            while ((strLine = dis.readLine()) != null) {
                parse.addLog(strLine);
            }
            parse.findWeek();
            parse.writeToCSV();
            fis.close();
            bis.close();
            dis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
