package ch.netwerkk.alemania;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Mischa.Imbiscuso on 15.12.2016.
 *
 * Klasse enthaelt die Methoden, welche die korrekten Date rausfiltert, in ein Array zwischen speichert
 * und die Daten dann in eine CSV Datei schreibt
 *
 */

public class ParseWeekdays {

    private ArrayList<String> logfile;
    private Date date;
    private ArrayList<String> matchedDates;
    private final String outputFile = "C:\\ScriptDoNotDelete\\output.csv";
    private int searchDays = 7;

    private static boolean writeCSVToConsole = true;
    private static boolean writeCSVToFile = true;
    private static boolean sortTheList = false;

    /**
     *  Konstruktor
     *  Es werden zwei Arrays erzeugt. Array lgofile enthaelt alle Strings aus der Logons.txt
     *  matchedDates enthaelt alle Strings, welche die Methode findWeek findet
     *
     *  mit date wird ein Datum von der Systemzeit erzeugt, welche bei der Ausfuehrung herrscht
     *
     *  Die Methode clearCSV loescht zuerst alle Eintraege in der CSV, damit immer nur die letzten
     *  7 Tage vorhanden sind
     *
     */
    public ParseWeekdays() {
        this.logfile = new ArrayList<>();
        this.matchedDates = new ArrayList<String>();
        this.date = new Date();
        this.clearCSV();
    }

    /**
     * Findet die Wochentage. Es wird ein Datum von der Ausfuehrungszeit genommen und in einer Vorschleife
     * immer ein Tag heruntergerechnet, bis auf 7 Tage.
     *
     * Mittels der Variabel SearchDays kann die Spannweite der Suche definiert werden.
     */
    public void findWeek() {
        String dateToFind = null;
        int searchDay = 0;
        Date searchDate = null;
        SimpleDateFormat ft = null;
        for (int i = 0; i <= this.searchDays; i++) {
            searchDate = new Date(this.date.getTime() - searchDay * 24 * 3600 * 1000);
            ft = new SimpleDateFormat("dd.MM.yyyy");
            dateToFind = ft.format(searchDate);
            for (String tmp : logfile) {

                if (tmp.contains(dateToFind)) {
                    this.matchedDates.add(tmp);

                }
            }
            searchDay++;
        }
    }


    /**
     * Schreibt das Array matchedDates in eine CSV File
     *
     * Ist die Datei nicht vorhanden, wird diese automatisch durch Java erstellt
     */
    public void writeToCSV() {
        String commaSeparatedValues = "";

        if (this.matchedDates != null) {
            if (this.sortTheList) {
                Collections.sort(this.matchedDates);
            }
            Iterator<String> iter = this.matchedDates.iterator();
            while (iter.hasNext()) {
                commaSeparatedValues += iter.next() + "\n";
            }
        }

        if (this.writeCSVToConsole) {
            System.out.println(commaSeparatedValues);
        }

        if (this.writeCSVToFile) {
            try {
                FileWriter fstream = new FileWriter(outputFile, true);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(commaSeparatedValues);
                out.close();
            } catch (Exception e) {

            }
        }
    }


    /**
     * Fuegt die Strings der logons.txt zum Aarray logfile
     * @param log
     */
    public void addLog(String log) {

        this.logfile.add(log);
    }


    /**
     * Temporaere Methode, welches den Inhalt des Arrays logfile auf die Konsole ausgibt
     */
    public void outputLog() {
        for (String tmp : this.logfile) {
            System.out.println(tmp);
        }
    }

    /**
     * Temporaere Methode, welches den Inhalt des Arrays matchedDates auf die Konsole ausgibt
     */
    public void outputMatchedDates() {
        for (String tmp : this.matchedDates) {
            System.out.println(tmp);
        }
    }

    /**
     * Loescht den Inhalt der csv Date. Ist diese nicht vorhanden, laeuft das Programm weiter
     */
    private void clearCSV() {

        try {
            FileWriter fw = new FileWriter(outputFile, false);
            PrintWriter pw = new PrintWriter(fw, false);
            pw.flush();
            pw.close();
            fw.close();
        } catch (Exception e) {

        }

    }

}
